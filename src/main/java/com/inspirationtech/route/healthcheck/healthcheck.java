package com.inspirationtech.route.healthcheck;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class healthcheck {

	@RequestMapping(path="/health")
	public String Status() {
		return "Health status: OK";
	}
}
